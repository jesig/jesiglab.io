const storeProducto = "";
let listaFiltrada;
let listaProducto;
let botonPrev;
let botonNext;

let parametroBusqueda = document.querySelector(".busqueda");

//variables para la paginacion
let pageNumber = 1;
const pageSize = 9;
let page;

window.onload = () => {
    fetch('https://jesig.herokuapp.com/magneto-jlig/producto/lista')
    // fetch('http://localhost:9090/magneto-jlig/producto/lista')
        .then(res => res.ok ? Promise.resolve(res) : Promise.reject(res))
        .then(res => res.json())
        .then(res => {

            this.storeProducto = res;
            this.listaProducto = document.getElementById('listaProducto');
            const fragment = document.createDocumentFragment();

            this.parametroBusqueda = document.querySelector(".busqueda");
            this.page = document.querySelector('.page');

            showItems();
            inicializarDatos();
        });
}

const inicializarDatos = () => {
    botonPrev = document.querySelector('.prev');
    botonNext = document.querySelector('.next');
    checkPaginador();
}

//se dibuja el card
const dibujarCard = (producto) => {
    const cardCreado = document.createElement('DIV');
    cardCreado.classList.add('card');

    const foto = buildFoto(producto);
    const descripcion = buildDescripcion(producto);

    cardCreado.appendChild(foto);
    cardCreado.appendChild(descripcion);

    return cardCreado;
}

// se dibuja la foto deltro del card
const buildFoto = (producto) => {
    const itemCreado = document.createElement('DIV');
    const imgCreado = document.createElement('IMG');

    imgCreado.src = producto.listFotos[0].foto;

    itemCreado.classList.add('item');
    itemCreado.appendChild(imgCreado);

    return itemCreado;
}

// se dibuja la descripcion dentro del card
const buildDescripcion = (producto) => {
    const descripcionCreado = document.createElement('DIV');
    const stok = document.createElement('LABEL');
    const descripcion = document.createElement('P');
    const marca = document.createElement('P');
    const precio = document.createElement('b');

    const btnDialog = document.createElement('button');
    btnDialog.className = "btn-detalle";
    btnDialog.textContent = "Detalle"
    
    btnDialog.addEventListener('click', () => {
        openDialog(producto);   
    });

    marca.classList.add('marca');

    producto.stock < 1 ? stok.textContent = "agotado" : stok.textContent = "Stock: " + producto.stock;
    descripcion.textContent = producto.descripcion;
    marca.textContent = producto.marca;
    precio.textContent = "S/" + producto.precioVenta + ".00";

    descripcionCreado.appendChild(stok);
    descripcionCreado.appendChild(descripcion);
    descripcionCreado.appendChild(marca);
    descripcionCreado.appendChild(precio);
    descripcionCreado.appendChild(btnDialog);

    descripcionCreado.classList.add('descripcion');

    return descripcionCreado;
}



//PAGINACION DE LOS CARDS DE PRODUCTOS

//EVALUAMOS QUE CARDS SE DEBEN MOSTRAR EN LA PAGINA
const showItems = () => {
    let galeria;

    filtrarLista();

    galeria = this.listaProducto.children;

    if (galeria != undefined && galeria.length > 0) {
        for (let i = 0; i < galeria.length; i++) {
            galeria[i].classList.remove("show");
            galeria[i].classList.add('hide');

            if (i >= (pageNumber * pageSize) - pageSize && i < pageNumber * pageSize) {
                galeria[i].classList.remove('hide');
                galeria[i].classList.add('show');
            }
        }
        this.page.textContent = "Page ";
        this.page.textContent += pageNumber;
    }
}

const filtrarLista = () => {
    this.listaFiltrada = [];
    if (this.parametroBusqueda.value != "") {
        let fragment = document.createDocumentFragment();
        this.listaFiltrada = this.storeProducto.filter(e => e.descripcion.toLowerCase().includes(this.parametroBusqueda.value.toLowerCase()));
        this.listaFiltrada.forEach(p => {
            let cardProducto = dibujarCard(p);
            fragment.appendChild(cardProducto);
        });

        resetearFiltros();

        this.listaProducto.appendChild(fragment);

    } else {
        let fragment = document.createDocumentFragment();

        this.storeProducto.forEach(p => {
            this.listaFiltrada.push(p)
        });

        for (const producto of this.listaFiltrada) {
            cardProducto = dibujarCard(producto);
            fragment.appendChild(cardProducto);
        }

        resetearFiltros();

        this.listaProducto.appendChild(fragment);
    }
}

const resetearFiltros = () => {
    while (this.listaProducto.childNodes.length >= 1) {
        this.listaProducto.removeChild(this.listaProducto.firstChild);
    }
}

const nextPage = () => {
    pageNumber++;
    checkPaginador();
    showItems();
}

const prevPage = () => {
    pageNumber--;
    checkPaginador();
    showItems();
}

const checkPaginador = () => {
    let paginacion = Math.ceil(this.listaFiltrada.length / pageSize);

    if (pageNumber == paginacion) {
        botonNext.classList.add('disabled');
    } else {
        botonNext.classList.remove('disabled');
    }

    if (pageNumber == 1) {
        botonPrev.classList.add('disabled');
    } else {
        botonPrev.classList.remove('disabled');
    }
}

// se dibuja el modal
const openDialog = (producto) => {
    let divPadre = document.createElement("div");
    divPadre.className = "modal-container ";
    divPadre.id="modal_container";

    let divHijo = document.createElement('div');
    divHijo.className = "modal";
    divHijo.id = "divHijo";       

    let content = document.createElement('div');
    content.className = "detalle-content";
    
    content.appendChild(dibujarFotosProducto(producto.listFotos));
    content.appendChild(dibujarDetalleProducto(producto));
    divHijo.appendChild(content);
    divHijo.appendChild(crearBotonCerrar());
    document.body.appendChild(divPadre);

    
    divPadre.appendChild(divHijo);
    
    showSlides(slideIndex);
}

// se cierra modal
const closeDialog = () => {
    let divPadre = document.getElementById('modal_container');
    document.body.removeChild(divPadre);

}
//detalle de producto
const dibujarFotosProducto = (listaFotos) => {
  
    let fotosDetalle = document.querySelector(".slideshow-container").cloneNode(true);
    

    for(let f of listaFotos){
        let mySlides = document.createElement('DIV');
        mySlides.className ="mySlides fade";

        let imgCreado = document.createElement('IMG');
        imgCreado.src = f.foto;
        imgCreado.className = "detalle-fotos";
        mySlides.appendChild(imgCreado);

        fotosDetalle.appendChild(mySlides);
    }    
   
    return fotosDetalle;
}

const dibujarDetalleProducto = (producto) => {
    let caracteristicas = document.createElement('DIV');
    caracteristicas.className = "detalle-caracteristicas";    
    caracteristicas.innerHTML = producto.foto;
    return caracteristicas;
}


let slideIndex = 1;

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function showSlides(n) {
    let i;
    let slides = document.getElementsByClassName("mySlides");
    
    if (n > slides.length) {slideIndex = 1}    
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";  
    }
  
    slides[slideIndex-1].style.display = "block";    
     
  }

  let crearBotonCerrar = () => {
    let btn = document.createElement('button');
    btn.textContent = "Cerrar";
    btn.className = "btn-cerrar-detalle";
    btn.onclick = closeDialog;

    return btn;
  }




